<?php

namespace LeadingCourses\BookingEngine;

final class IncompatibleVersionException extends \Exception
{
    public static function init(int $testerVersion, int $caseVersion): self
    {
        return new self(sprintf('This test-kit requires at least version %s of the contract-tester, while you have version %s. Please, update your contract-tester.', $caseVersion, $testerVersion));
    }
}
