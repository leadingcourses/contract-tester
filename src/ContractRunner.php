<?php

namespace LeadingCourses\BookingEngine;

use LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Contract;
use LeadingCourses\BookingEngine\Teetime\ContractManager\Exception\ContractException;

final class ContractRunner
{
    public static function runContract(Contract $contract, ContractTestCase $testCase): array
    {
        $period = new \DateInterval('P1D');

        $prices = [];

        foreach (new \DatePeriod($testCase->getStartDate(), $period, $testCase->getEndDate()) as $date) {
            $formattedDate = $date->format('Y-m-d');
            foreach ($testCase->getGames() as $i => $game) {
                foreach ($testCase->getTimes() as $time) {
                    try {
                        $prices[$formattedDate][$i][$time] = $contract->getProduct($game, $date->modify($time));
                    } catch (ContractException $e) {
                        $prices[$formattedDate][$i][$time] = $e;
                    }
                }
            }
        }

        return $prices;
    }
}
