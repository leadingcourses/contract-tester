<?php

namespace App\Teetime;

use LeadingCourses\BookingEngine\Teetime\TeetimeClub;

final class TeetimeCourse
{
    private $uniqueId;

    private $courseName;

    private $teetimeClub;

    public function __construct(string $uniqueId, TeetimeClub $teetimeClub, string $courseName = '')
    {
        $this->uniqueId = $uniqueId;
        $this->courseName = $courseName;
        $this->teetimeClub = $teetimeClub;
    }

    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    public function getTeetimeClub(): TeetimeClub
    {
        return $this->teetimeClub;
    }

    public function getCourseName(): string
    {
        return $this->courseName;
    }
}
