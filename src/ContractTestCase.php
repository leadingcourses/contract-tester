<?php

namespace LeadingCourses\BookingEngine;

final class ContractTestCase
{
    /**
     * @var array
     */
    private $games;
    /**
     * @var \DateTimeImmutable
     */
    private $startDate;
    /**
     * @var \DateTimeImmutable
     */
    private $endDate;
    /**
     * @var array
     */
    private $times;

    public function __construct(array $games, \DateTimeImmutable $startTime, \DateTimeImmutable $endTime, array $times = [])
    {
        $this->games = $games;
        $this->startDate = $startTime;
        $this->endDate = $endTime;
        $this->times = $times;
    }

    public function getGames(): array
    {
        return $this->games;
    }

    public function getStartDate(): \DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getEndDate(): \DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getTimes(): array
    {
        return $this->times ?? ['12:00'];
    }
}
