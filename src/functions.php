<?php

use LeadingCourses\BookingEngine\IncompatibleVersionException;
use Symfony\Component\Finder\Finder;

function findContracts(): array
{
    $finder = new Finder();

    $finder->in('Contract')->files()->depth(1)->name('*.php');

    $contracts = [];

    foreach ($finder as $file) {
        $contracts[$file->getRelativePath()][] = $file->getFilenameWithoutExtension();
    }

    return $contracts;
}

/**
 * @param int $minimumVersion
 *
 * @throws IncompatibleVersionException
 */
function assertContractTesterVersionIsAtLeast(int $minimumVersion): void
{
    $contractTesterVersion = 4;

    if ($contractTesterVersion < $minimumVersion) {
        throw IncompatibleVersionException::init($contractTesterVersion, $minimumVersion);
    }
}
