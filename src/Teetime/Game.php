<?php

namespace LeadingCourses\BookingEngine\Teetime;

use App\Teetime\TeetimeCourse;

final class Game
{
    private $id;

    private $nrHoles;

    /** @var TeetimeCourse The main course on which the game is played */
    private $primaryCourse;

    /**
     * @var array keyed array of provider-specific values strictly meant for usages inside the API clients
     */
    private $providerSpecifics;
    /**
     * @var TeetimeCourse
     */
    private $secondaryCourse;

    public function getId()
    {
        return $this->id;
    }

    public function __construct(int $nrHoles, TeetimeCourse $primaryCourse, TeetimeCourse $secondaryCourse = null, array $providerSpecifics = [])
    {
        $this->nrHoles = $nrHoles;
        $this->primaryCourse = $primaryCourse;
        $this->secondaryCourse = $secondaryCourse;
        $this->providerSpecifics = $providerSpecifics;
    }

    public function getNumberOfHoles(): int
    {
        return $this->nrHoles;
    }

    public function getPrimaryCourse(): TeetimeCourse
    {
        return $this->primaryCourse;
    }

    public function getSecondaryCourse(): ?TeetimeCourse
    {
        return $this->secondaryCourse;
    }

    public function getProviderSpecificData(): array
    {
        return $this->providerSpecifics;
    }

    /**
     * @return mixed
     */
    public function getProviderSpecificDataValue(string $name, $default = null)
    {
        return $this->providerSpecifics[$name] ?? $default;
    }

    public function is9Holes(): bool
    {
        return $this->getNumberOfHoles() === 9;
    }

    public function is18Holes(): bool
    {
        return $this->getNumberOfHoles() === 18;
    }

    public function getClub(): \Club
    {
        return $this->getPrimaryCourse()->getTeetimeClub()->getClub();
    }

    public function getCourseNames(): string
    {
        return $this->primaryCourse->getCourseName().($this->secondaryCourse ? ' + '.$this->secondaryCourse->getCourseName() : '');
    }
}
