<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager;

use Assert\Assert;
use Money\Currency;
use Money\Money;

final class Price
{
    /**
     * @var int
     */
    private $tourOperatorRate;
    /**
     * @var int
     */
    private $sellingPrice;
    /**
     * @var int
     */
    private $rackRate;
    /**
     * @var Currency
     */
    private $currency;

    public function __construct(
        int $tourOperatorRate,
        int $sellingPrice,
        int $rackRate,
        Currency $currency,
        int $payableOnline = null
    ) {
        Assert::that($tourOperatorRate)->lessOrEqualThan($sellingPrice, sprintf('TourOperator rate (%d) should be equal to or less than selling price (%d)', $tourOperatorRate, $sellingPrice));
        Assert::that($rackRate)->greaterOrEqualThan($sellingPrice, sprintf('Rack rate (%d) should be equal to or bigger than selling price (%d)', $rackRate, $sellingPrice));
        Assert::that($payableOnline)->nullOr()->lessOrEqualThan($sellingPrice)->greaterOrEqualThan($sellingPrice - $tourOperatorRate, 'Payable online must cover the commission at least');

        $this->tourOperatorRate = $tourOperatorRate;
        $this->sellingPrice = $sellingPrice;
        $this->rackRate = $rackRate;
        $this->currency = $currency;
        $this->payableOnline = $payableOnline ?? $sellingPrice;
    }

    public function getTourOperatorRate(): Money
    {
        return new Money($this->tourOperatorRate, $this->currency);
    }

    public function getSellingPrice(): Money
    {
        return new Money($this->sellingPrice, $this->currency);
    }

    public function getRackRate(): Money
    {
        return new Money($this->rackRate, $this->currency);
    }

    public function getPayableOnline(): Money
    {
        return new Money($this->payableOnline, $this->currency);
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function getDiscount(): Money
    {
        return new Money($this->rackRate - $this->sellingPrice, $this->currency);
    }

    public function getCommission(): Money
    {
        return new Money($this->sellingPrice - $this->tourOperatorRate, $this->currency);
    }

    public function multiply(float $multiplier): self
    {
        return new self(
            $this->round($this->tourOperatorRate * $multiplier),
            $this->round($this->sellingPrice * $multiplier),
            $this->round($this->rackRate * $multiplier),
            $this->currency,
            $this->round($this->payableOnline * $multiplier)
        );
    }

    public function add(self $other): self
    {
        $this->assertSameCurrency($other);

        return new self((int) $other->getTourOperatorRate()->getAmount() + $this->tourOperatorRate,
            (int) $other->getSellingPrice()->getAmount() + $this->sellingPrice,
            (int) $other->getRackRate()->getAmount() + $this->rackRate,
            $this->currency,
            (int) $other->getPayableOnline()->getAmount() + $this->payableOnline
        );
    }

    /**
     * @param float $multiplier
     *
     * @return float
     */
    private function round(float $value): int
    {
        return (int) round($value, 0);
    }

    private function assertSameCurrency(self $other): void
    {
        if (!$this->currency->equals($other->getCurrency())) {
            throw new \InvalidArgumentException('Cannot operate on a price of a different currency!');
        }
    }
}
