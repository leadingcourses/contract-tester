<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Exception;

use LeadingCourses\BookingEngine\Teetime\Game;

final class CourseIsClosedException extends ContractException
{
    protected static function getErrorMessage(string $contractIdentifier, Game $game, \DateTimeImmutable $time): string
    {
        return sprintf(
            'Game %s (at %s) was found for closed course: %s.',
            $game->getId(),
            $time->format('Y-m-d'),
            $contractIdentifier
        );
    }
}
