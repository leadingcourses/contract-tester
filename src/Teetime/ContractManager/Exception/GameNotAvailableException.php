<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Exception;

use LeadingCourses\BookingEngine\Teetime\Game;

final class GameNotAvailableException extends ContractException
{
    protected static function getErrorMessage(string $contractIdentifier, Game $game, \DateTimeImmutable $time): string
    {
        return sprintf(
            'Game %s is not available at %s for contract %s.',
            $game->getId(),
            $time->format('Y-m-d'),
            $contractIdentifier
        );
    }
}
