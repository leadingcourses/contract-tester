<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Exception;

use LeadingCourses\BookingEngine\Teetime\Game;

abstract class ContractException extends \DomainException
{
    public static function init(
        string $contractIdentifier,
        Game $game,
        \DateTimeImmutable $time
    ): self {
        return new static(sprintf(static::getErrorMessage($contractIdentifier, $game, $time)));
    }

    abstract protected static function getErrorMessage(string $contractIdentifier, Game $game, \DateTimeImmutable $time): string;
}
