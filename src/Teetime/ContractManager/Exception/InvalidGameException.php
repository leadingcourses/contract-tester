<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Exception;

use LeadingCourses\BookingEngine\Teetime\Game;

final class InvalidGameException extends ContractException
{
    protected static function getErrorMessage(string $contractIdentifier, Game $game, \DateTimeImmutable $time): string
    {
        return sprintf(
            'Contract %s does not have prices defined for the contents of game %s, requested at %s.',
            $contractIdentifier,
            $game->getId(),
            $time->format('Y-m-d')
        );
    }
}
