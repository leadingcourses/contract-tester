<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Exception;

use LeadingCourses\BookingEngine\Teetime\Game;

final class ContractDoesntApplyException extends ContractException
{
    protected static function getErrorMessage(string $contractIdentifier, Game $game, \DateTimeImmutable $time): string
    {
        return sprintf(
            "Contract %s doesn't apply to game %s, at %s",
            $contractIdentifier,
            $game->getId(),
            $time->format('c')
        );
    }
}
