<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract;

use LeadingCourses\BookingEngine\Teetime\ContractManager\Product;
use LeadingCourses\BookingEngine\Teetime\Game;

interface Contract extends PricingRule
{
    public function getProduct(
        Game $game,
        \DateTimeImmutable $startTime
    ): Product;
}
