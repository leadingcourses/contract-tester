<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Helper;

trait CancellationDateHelper
{
    private function getDefaultCancellationDeadline(\DateTimeImmutable $startTime): \DateTimeImmutable
    {
        return $startTime->modify('-2 days');
    }

    private function getCancellationDeadline(\DateTimeImmutable $startTime, int $cancellationPeriodInHours): \DateTimeImmutable
    {
        return $startTime->modify(sprintf('-%d hours', $cancellationPeriodInHours));
    }
}
