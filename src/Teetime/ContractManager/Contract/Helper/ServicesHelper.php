<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Helper;

use LeadingCourses\BookingEngine\Teetime\Game;
use LeadingCourses\BookingEngine\Teetime\IncludedService;

trait ServicesHelper
{
    protected function getServicesFromGame(Game $game, bool $isTwilight = false): array
    {
        $services = [];

        if ($greenfees = $this->getNumberOfGreenFeesInGame($game)) {
            if ($isTwilight) {
                $services[] = IncludedService::twilightGreenFee($greenfees);
            } else {
                $services[] = IncludedService::greenFee($greenfees);
            }
        }

        if ($buggies = $this->getNumberOfBuggiesInGame($game)) {
            $services[] = IncludedService::buggy($buggies);
        }

        if ($trolleys = $this->getNumberOfTrolleysInGame($game)) {
            $services[] = IncludedService::trolley($trolleys);
        }

        if ($electricTrolleys = $this->getNumberOfElectricTrolleysInGame($game)) {
            $services[] = IncludedService::electricTrolley($electricTrolleys);
        }

        return $services;
    }

    abstract protected function getNumberOfGreenFeesInGame(Game $game): int;

    abstract protected function getNumberOfBuggiesInGame(Game $game): float;

    abstract protected function getNumberOfTrolleysInGame(Game $game): float;

    abstract protected function getNumberOfElectricTrolleysInGame(Game $game): float;
}
