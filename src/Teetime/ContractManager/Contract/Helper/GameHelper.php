<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Helper;

trait GameHelper
{
    private function isEvenGreenFeesAndBuggies(int $greenFees, float $buggies): bool
    {
        return $greenFees % 2 === 0 && $buggies == $greenFees / 2;
    }
}
