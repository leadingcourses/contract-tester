<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Helper;

use Assert\Assert;

trait DateHelper
{
    /**
     * This function *does* return true for the given time. This is used when
     * contracts define 'twilight greenfees' as of 16:00 for example, meaning
     * that at 16:00 exactly the first twilight greenfee will be available.
     */
    protected function isTimeFrom(\DateTimeImmutable $date, int $hours, int $minutes): bool
    {
        return $date >= $date->setTime($hours, $minutes);
    }

    /**
     * This function *does not* include the given time itself. The reason for
     * this is that contracts define 'early bird greenfees' up till 8:00 for example,
     * meaning that at 8:00 the first regular greenfee is available.
     */
    protected function isTimeBefore(\DateTimeImmutable $date, int $hours, int $minutes): bool
    {
        return $date < $date->setTime($hours, $minutes);
    }

    protected function isDateBetween(\DateTimeImmutable $date, $lowerLimit, $upperLimit): bool
    {
        $lowerLimit = $this->tryGetDate($lowerLimit)->setTime(0, 0, 0);
        $upperLimit = $this->tryGetDate($upperLimit)->setTime(23, 59, 59);
        Assert::that($lowerLimit)->lessThan($upperLimit);

        return $date >= $lowerLimit && $date <= $upperLimit;
    }

    protected function isTimeBetween(\DateTimeImmutable $date, int $lowerHour, int $lowerMinute, int $upperHour, int $upperMinute): bool
    {
        return $date >= $date->setTime($lowerHour, $lowerMinute) && $date <= $date->setTime($upperHour, $upperMinute);
    }

    protected function isWeekend(\DateTimeImmutable $startTime): bool
    {
        return \in_array($startTime->format('N'), [6, 7], false);
    }

    private function tryGetDate($date): \DateTimeImmutable
    {
        if ($date instanceof \DateTimeImmutable) {
            return $date;
        }

        if ($date instanceof \DateTime) {
            return \DateTimeImmutable::createFromMutable($date);
        }

        if (\is_string($date)) {
            return new \DateTimeImmutable($date);
        }

        throw new \InvalidArgumentException(sprintf('Could not convert %s to a valid DateTimeImmutable', print_r($date, true)));
    }

    private function isInMonth(\DateTimeImmutable $startTime, array $months): bool
    {
        return \in_array((int) $startTime->format('n'), $months, true);
    }
}
