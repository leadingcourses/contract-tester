<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Helper;

trait MultiplayerHelper
{
    private $numberOfPlayers;

    public function setNumberOfPlayers(int $numberOfPlayers): void
    {
        $this->numberOfPlayers = $numberOfPlayers;
    }
}
