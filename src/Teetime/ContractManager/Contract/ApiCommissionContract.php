<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract;

use LeadingCourses\BookingEngine\Teetime\ContractManager\Product;
use LeadingCourses\BookingEngine\Teetime\Game;
use Money\Money;

interface ApiCommissionContract extends PricingRule
{
    public function getProduct(
        Game $game,
        \DateTimeImmutable $startTime,
        Money $apiPrice,
        Money $rackPrice
    ): Product;
}
