<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract;

interface MultiPlayerContract extends PricingRule
{
    public function setNumberOfPlayers(int $numberOfPlayers): void;
}
