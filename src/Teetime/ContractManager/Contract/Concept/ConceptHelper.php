<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Concept;

use LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Helper\ServicesHelper;
use LeadingCourses\BookingEngine\Teetime\Game;

trait ConceptHelper
{
    use ServicesHelper;

    protected function getNumberOfGreenFeesInGame(Game $game): int
    {
        $services = $game->getProviderSpecificDataValue('services', []);

        if (!isset($services['greenfee']) || (!ctype_digit($services['greenfee']) && !\is_int($services['greenfee']))) {
            throw new \RuntimeException(sprintf('Game with ID %s does not have a valid value for services > greenfee set.', $game->getId()));
        }

        return $services['greenfee'];
    }

    protected function getNumberOfBuggiesInGame(Game $game): float
    {
        $services = $game->getProviderSpecificDataValue('services', []);

        if (!isset($services['buggy']) || !is_numeric($services['buggy'])) {
            throw new \RuntimeException(sprintf('Game with ID %s does not have a valid value for services > buggy set.', $game->getId()));
        }

        return $services['buggy'];
    }

    protected function getNumberOfTrolleysInGame(Game $game): float
    {
        return 0;
    }

    protected function getNumberOfElectricTrolleysInGame(Game $game): float
    {
        return 0;
    }

    protected function getMType(Game $game): int
    {
        return (int) $game->getProviderSpecificDataValue('memberType', 0);
    }
}
