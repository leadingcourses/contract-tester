<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Chronogolf;

use LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Helper\ServicesHelper;
use LeadingCourses\BookingEngine\Teetime\Game;

trait ChronogolfHelper
{
    use ServicesHelper;

    protected function getNumberOfGreenFeesInGame(Game $game): int
    {
        return $game->getProviderSpecificDataValue('greenFees');
    }

    protected function getNumberOfBuggiesInGame(Game $game): float
    {
        return $game->getProviderSpecificDataValue('buggies');
    }

    protected function getNumberOfTrolleysInGame(Game $game): float
    {
        return 0;
    }

    protected function getNumberOfElectricTrolleysInGame(Game $game): float
    {
        return 0;
    }
}
