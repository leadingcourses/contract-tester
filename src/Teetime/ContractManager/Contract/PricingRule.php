<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract;

use LeadingCourses\BookingEngine\Teetime\Game;

interface PricingRule
{
    public function appliesTo(Game $game, \DateTimeImmutable $startTime): bool;
}
