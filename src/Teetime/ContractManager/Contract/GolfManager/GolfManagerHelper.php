<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\GolfManager;

use LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Helper\ServicesHelper;
use LeadingCourses\BookingEngine\Teetime\Game;
use LeadingCourses\BookingEngine\Teetime\IncludedService;

trait GolfManagerHelper
{
    use ServicesHelper;

    protected function getNumberOfGreenFeesInGame(Game $game): int
    {
        return 1;
    }

    protected function getNumberOfBuggiesInGame(Game $game): float
    {
        if (\in_array('buggy', $game->getProviderSpecificDataValue('tags'), true)) {
            if ($game->getProviderSpecificDataValue('multiple') === 2) {
                return 0.5;
            }

            return 1;
        }

        return 0;
    }

    protected function getNumberOfElectricTrolleysInGame(Game $game): int
    {
        return \in_array('electric-trolley', $game->getProviderSpecificDataValue('tags'), true)
            ? $this->getNumberOfGreenFeesInGame($game)
            : 0;
    }

    protected function getNumberOfTrolleysInGame(Game $game): int
    {
        return \in_array('trolley', $game->getProviderSpecificDataValue('tags'), true)
            ? $this->getNumberOfGreenFeesInGame($game)
            : 0;
    }

    /**
     * You can override the amount of buggies in a game, if you know the API is incorrect.
     *
     * @return IncludedService[]
     */
    private function getServicesFromGameWithBuggyOverride(Game $game, float $hardcodedBuggies = null): array
    {
        $services = $this->getServicesFromGame($game);
        if (isset($hardcodedBuggies)) {
            $services = $this->overrideBuggies($services, $hardcodedBuggies);
        }

        return $services;
    }

    /**
     * @param IncludedService[] $services
     *
     * @return IncludedService[]
     */
    private function overrideBuggies(array $services, float $override): array
    {
        foreach ($services as $service) {
            if ($service->isBuggy()) {
                $service->multiply(1 / $service->getAmount());
            }
        }

        return $services;
    }
}
