<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Imastergolf;

use LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\Helper\ServicesHelper;
use LeadingCourses\BookingEngine\Teetime\Game;

trait ImasterGolfHelper
{
    use ServicesHelper;

    protected function getNumberOfGreenFeesInGame(Game $game): int
    {
        $rateTypeId = $game->getProviderSpecificDataValue('rateTypeId');
        switch ($rateTypeId) {
            case '1':
            case '2':
            case '9':
                return 1;
            case '3':
                return 2;
            case '4':
                return 4;
        }

        throw new \RuntimeException('Cannot determine number of greenfees in rateTypeId '.$rateTypeId);
    }

    protected function getNumberOfBuggiesInGame(Game $game): float
    {
        $rateTypeId = $game->getProviderSpecificDataValue('rateTypeId');

        switch ($rateTypeId) {
            case '3':
            case '9':
                return 1;
            case '4':
                return 2;
        }

        return 0;
    }

    protected function getNumberOfTrolleysInGame(Game $game): float
    {
        return 0;
    }

    protected function getNumberOfElectricTrolleysInGame(Game $game): float
    {
        return 0;
    }
}
