<?php

namespace LeadingCourses\BookingEngine\Teetime\ContractManager;

use LeadingCourses\BookingEngine\Teetime\Game;
use LeadingCourses\BookingEngine\Teetime\IncludedService;
use Assert\Assert;

final class Product
{
    /**
     * @var Game
     */
    private $game;
    /**
     * @var Price
     */
    private $price;
    /**
     * @var IncludedService[]
     */
    private $includedServices;
    /**
     * @var array
     */
    private $notes;
    /**
     * @var \DateTimeImmutable
     */
    private $cancellationDeadline;

    public function __construct(Game $game, Price $price, array $services, ?\DateTimeImmutable $cancellationDeadline, array $notes = [])
    {
        Assert::thatAll($services)->isInstanceOf(IncludedService::class);
        $this->game = $game;
        $this->price = $price;
        $this->includedServices = $services;
        // cancellation deadlines can be in the past, e.g. when a user is booking something same day or so.
        $this->cancellationDeadline = $cancellationDeadline;
        $this->notes = $notes;
    }

    public function getGame(): Game
    {
        return $this->game;
    }

    public function getPrice(): Price
    {
        return $this->price;
    }

    /**
     * @return IncludedService[]
     */
    public function getIncludedServices(): array
    {
        return $this->includedServices;
    }

    public function getNotes(): array
    {
        return $this->notes;
    }

    public function withExtraService(IncludedService $addedService, Price $price, array $notes = []): self
    {
        $newPrice = $this->price->add($price);
        $newNotes = array_merge($this->notes, $notes);
        $newServices = [];

        foreach ($this->includedServices as $existingService) {
            if ($addedService->isSameTypeAs($existingService)) {
                $addedService = $addedService->merge($existingService);
                // will add later
                continue;
            }

            $newServices[] = $existingService;
        }

        $newServices[] = $addedService;

        return new self($this->game, $newPrice, $newServices, $this->cancellationDeadline, $newNotes);
    }

    public function getMultiple($multiplier): self
    {
        $newPrice = $this->price->multiply($multiplier);
        $newServices = [];

        foreach ($this->includedServices as $includedService) {
            $newServices[] = $includedService->multiply($multiplier);
        }

        return new self($this->game, $newPrice, $newServices, $this->cancellationDeadline, $this->notes);
    }

    public function canBeSoldForNumberOfPlayers(int $numberOfPlayers): bool
    {
        return $numberOfPlayers % $this->getNumberOfPlayers() === 0;
    }

    public function getNumberOfPlayers(): int
    {
        foreach ($this->getIncludedServices() as $service) {
            if ($service->isGreenFee()) {
                return $service->getAmount();
            }
        }

        return 0;
    }

    public function getMultipleForNumberOfPlayers(int $desiredNumberOfPlayers): self
    {
        $multiple = $desiredNumberOfPlayers / $this->getNumberOfPlayers();

        if (!\is_int($multiple)) {
            throw new \DomainException(sprintf('Product with %d greenfees cannot be sold to %d users, please use canBeSoldForNumberOfPlayers before calling getMultipleForNumberOfPlayers', $this->getNumberOfPlayers(), $desiredNumberOfPlayers));
        }

        return $this->getMultiple($multiple);
    }

    public function hasCancellationDeadline(): bool
    {
        return $this->cancellationDeadline !== null;
    }

    public function getCancellationDeadline(): ?\DateTimeImmutable
    {
        return $this->cancellationDeadline;
    }
}
