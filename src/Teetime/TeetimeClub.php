<?php

namespace LeadingCourses\BookingEngine\Teetime;

final class TeetimeClub
{
    private $uniqueId;

    private $commissionPercentage;

    private $clubId;

    public function __construct(
        string $uniqueId,
        int $clubId,
        ?float $commissionPercentage = null
    ) {
        $this->uniqueId = $uniqueId;
        $this->clubId = $clubId;
        $this->commissionPercentage = $commissionPercentage;
    }

    public function getClub(): ?\Club
    {
        return new \Club($this->clubId);
    }

    public function getCommissionPercentage(): ?float
    {
        return $this->commissionPercentage;
    }

    public function getUniqueId(): string
    {
        return $this->uniqueId;
    }
}
