<?php

namespace LeadingCourses\BookingEngine\Teetime;

use Assert\Assert;

final class IncludedService
{
    private const GREEN_FEE = 'greenfee';
    private const TWILIGHT_GREEN_FEE = 'twilight';
    private const BUGGY = 'buggy';
    private const ELECTRIC_TROLLEY = 'electric-trolley';
    private const TROLLEY = 'trolley';

    private static $known_services = [
        self::GREEN_FEE,
        self::TWILIGHT_GREEN_FEE,
        self::BUGGY,
        self::ELECTRIC_TROLLEY,
        self::TROLLEY,
    ];

    private $amount;

    private $type;

    private function __construct(string $id, $amount)
    {
        Assert::that($id)->inArray(self::$known_services);
        Assert::that($amount)->greaterThan(0);
        $this->type = $id;
        $this->amount = $amount;
    }

    public static function greenFee(int $amount): self
    {
        return new self(self::GREEN_FEE, $amount);
    }

    public static function twilightGreenFee(int $amount): self
    {
        return new self(self::TWILIGHT_GREEN_FEE, $amount);
    }

    public static function buggy(float $amount): self
    {
        return new self(self::BUGGY, $amount);
    }

    public static function ElectricTrolley(int $amount): self
    {
        return new self(self::ELECTRIC_TROLLEY, $amount);
    }

    public static function Trolley(int $amount): self
    {
        return new self(self::TROLLEY, $amount);
    }

    public function getAmount(): float
    {
        return (float) $this->amount;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isSameTypeAs(self $otherService): bool
    {
        return $this->type === $otherService->getType();
    }

    public function merge(self $otherService): self
    {
        if (!$this->isSameTypeAs($otherService)) {
            throw new \InvalidArgumentException(sprintf('cannot merge two services of types %s and %s', $this->type, $otherService->getType()));
        }

        return new self($this->type, $this->amount + $otherService->getAmount());
    }

    public function multiply($multiplier): self
    {
        if (!is_numeric($multiplier)) {
            throw new \InvalidArgumentException('IncludedService::multiply expects a numeric value, got'.$multiplier);
        }

        if ($this->isGreenFee() && !ctype_digit((string) $multiplier)) {
            throw new \DomainException('Cannot multiply green fee by non-integer multiplier '.$multiplier);
        }

        return new self($this->type, $this->amount * $multiplier);
    }

    public function isGreenFee(): bool
    {
        return \in_array($this->type, [self::GREEN_FEE, self::TWILIGHT_GREEN_FEE], true);
    }

    public function isBuggy(): bool
    {
        return $this->type === self::BUGGY;
    }

    public function isElectricTrolley(): bool
    {
        return $this->type === self::ELECTRIC_TROLLEY;
    }

    public function isTrolley(): bool
    {
        return $this->type === self::TROLLEY;
    }
}
