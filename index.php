<?php

require_once __DIR__.'/vendor/autoload.php';

use LeadingCourses\BookingEngine\ContractRunner;

try {
    if (!isset($_GET['contract'])) {
        throw new RuntimeException('Please select a contract.');
    }

    list($group, $contractName) = explode('\\', $_GET['contract']);

    $testCasePath = __DIR__.'/test-cases/'.$contractName.'.php';

    if (!file_exists($testCasePath)) {
        throw new RuntimeException(sprintf('Test case for <b>%s</b> cannot be found!', $contractName));
    }

    $contractClassName = sprintf('LeadingCourses\BookingEngine\Teetime\ContractManager\Contract\%s\%s', $group, $contractName);
    $contract = new $contractClassName();
    $testCase = require $testCasePath;

    $prices = ContractRunner::runContract($contract, $testCase);
} catch (\Throwable $e) {
    $error = $e->getMessage();
}

$contracts = findContracts();
include_once __DIR__.'/view.php';
