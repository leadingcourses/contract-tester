# Leadingcourses Contract Tester

Leadingcourses Contract Tester is an environment for developing and testing leadingcourses tee-time contracts.

## Initial setup

To run this tool, you need at least PHP7.4 and composer installed.

1. install required packages by running
    ```bash
    composer install
    ```
2. run PHP development server (or any other server you prefer)
    ```bash
    php -S localhost:9090
    ``` 
3. visit the testing tool at http://localhost:9090


## Handling new contract tasks

1. You'll receive a zip archive `<some_name>.zip` from leading courses, it contains two files, `Contract/<some_directory>/<some_name>.php` and `test-cases/<some_name>.php` 
2. unzip the archive in the same directory as this README file, the two files should end up in the respective directories in this project (if you're running linux, you can do `unzip <some_name>.zip`, otherwise just unzip the archive and copy the files to the correct places.)
3. if the new contract is installed correctly, you should see it in the drop down list on the top left-corner of the testing tool.
4. open your newly-created contract template under `Contract/<some_directory>/<some_name>.php` and fill it with the correct prices (you can always refresh the testing tool to see prices reflect)
5. run `bin/php-cs-fixer fix` to make sure your new contract matches our code-style requirements.
6. once you're done with the development of your new contract and can see that all dates and games in the tool are reflecting the correct prices, send us back the Contract file `Contract/<some_directory>/<some_name>.php`

## Example / Sample Club ##

As tutorial we've included [a sample contract](SampleClub contract.pdf) and [a sample archive](SampleClub.zip). Please implement the contract according to the steps above and send it back before you will be handed real-world contracts.  

## Technical notes on how our contracts work.

A contract is a simple PHP class that is responsible for telling our system what to sell and how much it will cost, based on a given `Game` at a certain point in time.

### `Game` Class
a `Game` describes a certain playing configuration or a "round of golf" for example, how many golfers can play this game (if it's a package) or how many holes there are and on which course is the golfer going to play, or whether a buggy is included .. 

### `Price` Class
a `Price` is a [value object](https://martinfowler.com/bliki/ValueObject.html) that holds four values:
 1. `toRate`: the amount LC pays to the golf course, represented as number of cents
 2. `sellingPrice`: the amount the golfer pays to LC, represented as number of cents
 3. `rackRate`: the original price of the game before any discounts, represented as number of cents
 4. `currency`: a [`Money\Currency`](https://github.com/moneyphp/money/blob/master/src/Currency.php) object indicating what currency those prices are in (usually euro)
 
### `IncludedService` Class
IncludedServices are a standardized way to express what we're going to actually sell to the golfer like (twilight) green-fees and buggies, you can generally create new objects of this type by calling named constructors like `IncludedService::buggy(0.5)` (more on how to use it below)
 
### `Product` Class
a product is what we will end up selling to the golfers on the website, it's a combination of:
1. a `Game`, same as what's passed to `Contract::getProduct()` 
2. a `Price` object
3. an array of `IncludedService`: how many (twilight) green-fees, buggies are included in that price
4. an optional `cancellationDate`, `DateTimeImmutable` for when is the latest date this product can be cancelled, or null if the product cannot be cancelled. 
5. an optional array of `notes` (strings) to be shown to the golfer about the teetime, please don't add any notes unless explicitly asked to.

### `Contract::getProduct()` method
Technically speaking, `Contract::getProduct(Game $game, \DateTimeImmutable $startTime): Product` is where all the actual pricing happens, it takes a `Game` and a `DateTimeImmutable` and returns a `Product`.
 

### `Contract::appliesTo()` method
This method is the entry point to our contracts, it should return true if this contract can handle (provide a price) for a given game and date. Ideally you should not need to touch this method, it will be handled for you.
