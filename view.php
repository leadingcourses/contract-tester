<?php
use LeadingCourses\BookingEngine\Teetime\ContractManager\Product;use LeadingCourses\BookingEngine\Teetime\Game;
use Money\Money;

function formatMoney(Money $money): string
{
    $currencySymbols = [
        'USD' => '$',
        'EUR' => '€',
    ];

    $currencyCode = $money->getCurrency()
        ->getCode();

    return ($currencySymbols[$currencyCode] ?? $currencyCode).' '.($money->getAmount() / 100);
}

function getClassBaseName(object $object): string {
    $classname = get_class($object);
    return (substr($classname, strrpos($classname, '\\') + 1));
}
?><!doctype HTML>
<html lang="en">
<head>
    <title>LC Contract Tester</title>
    <link rel="stylesheet" href="//www.leadingcourses.com/css/structure/main.min.css" />
    <link rel="stylesheet" href="//www.leadingcourses.com/css/components/common-elements.min.css" />
    <link rel="stylesheet" href="//www.leadingcourses.com/css/components/forms.min.css" />
    <style>
        .main--form {
            display: flex;
            padding: 1rem;
        }
        .main--form .lc-button {
            margin-left: 1rem;
        }
        .game__time {
            font-family: 'lucida console', monospace;
            white-space: nowrap;
        }
        .element--table .game__date {
            font-family: 'lucida console', monospace;
            min-width: 0;
            padding: .5em .7em
        }
        .element--table tr:nth-child(odd) {
            background: #fff;
        }
        .element--table tr:nth-child(even) {
            background: #ccc;
        }
        .element--table thead th {
            position: sticky;
            top: 0;
            z-index: 2;
            background-color: #fff;
            box-shadow: 0 1px 0 #666;
        }
        .game__services li {
            list-style: none;
        }
    </style>
</head>
<body>
<?php if (!empty($contracts)): ?>
    <form method="get" class="main--form">
        <label class="form--select">
            <select name="contract">
                <?php foreach ($contracts as $path => $contractsInPath): ?>
                    <optgroup label="<?= $path; ?>">
                        <?php foreach ($contractsInPath as $contract): ?>
                            <?php $value = $path.'\\'.$contract; ?>
                            <option value="<?= $value; ?>" <?= ($_GET['contract'] ?? '') === $value ? 'selected' : ''; ?> ><?= $contract; ?></option>
                        <?php endforeach; ?>
                    </optgroup>
                <?php endforeach; ?>
            </select>
        </label>
        <button class="lc-button">Test ❯</button>
    </form>
<?php else: ?>
    <p>No contracts found yet. Contact Leadingcourses to receive them, or download the sample from <a target="_blank" href="https://gitlab.com/leadingcourses/contract-tester/#example-sample-club">gitlab</a>.</p>
<?php endif; ?>
<?php if (isset($error)): ?>
    <?= $error; ?>
<?php else: ?>
    <table class="element--table">
        <thead>
        <tr>
            <th>Date</th>
            <?php
            /** @var Game $game */
            foreach ($games as $i => $game):
                foreach ($times as $time): ?>
                    <th title='<?= json_encode($game->getProviderSpecificData()); ?>'>#<?= $i + 1; ?> <?= $game->getCourseNames(); ?> (<?= $game->getNumberOfHoles(); ?>h) at <?= $time; ?></th>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($prices as $date => $gameProducts): ?>
            <tr>
                <th class="game__date"><?= $date; ?></th>
                <?php
                foreach ($gameProducts as $i => $times):
                    foreach ($times as $time => $product): ?>
                        <td class="game__time">
                            <?php if ($product instanceof Product): ?>
                                TO: <?= formatMoney($product->getPrice()->getTourOperatorRate()); ?> <br/>
                                SR: <?= formatMoney($product->getPrice()->getSellingPrice()); ?> <br/>
                                RR: <?= formatMoney($product->getPrice()->getRackRate()); ?> <br/>
                                <div class="game__services">
                                    Services: <li><?= implode('<li>', array_map(static function (LeadingCourses\BookingEngine\Teetime\IncludedService $service) { return '&nbsp;'.$service->getAmount().'&times; '.$service->getType(); }, $product->getIncludedServices())) ?: '!!!NO SERVICES!!!'; ?>
                                </div>
                                <?php if ($product->getNotes()): ?>
                                    <?= json_encode($product->getNotes()); ?>
                                <?php endif; ?>
                                <?php if ($product->hasCancellationDeadline()): ?>
                                    Cancel before: <?= $product->getCancellationDeadline()->format('Y-m-d'); ?><br/>
                                <?php endif; ?>
                            <?php elseif ($product instanceof \Throwable): ?>
                                <em><?= getClassBaseName($product); ?></em>
                            <?php else: ?>
                                Contract doesn't apply!
                            <?php endif; ?>
                        </td>
                    <?php
                    endforeach;
                endforeach;
                ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
</body>
</html>
